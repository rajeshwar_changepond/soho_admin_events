Rails.application.routes.draw do
  root to: 'dashboard#index'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_for :customers

  resources :customers, :customer_personal_infos, :events, :event_categories, :customer_events

  # Api paths
  mount SohoApi::Base => '/'
  mount GrapeSwaggerRails::Engine => '/swagger'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
