# README
# Soho Admin POC

## Setup
Kindly run `rake db:seed` to setup data

## App

This app have 2 portal admin and customer, both will have respective login pages. Login credentials are in seeds.

This app support API by using ruby-grape for defining API and grape-entity for data customization.
run `rake grape:routes` to list all API routes.

Swagger included, you can even go to `localhost:port/swagger_doc`

## Testcase

Handled testcase for all contoller spec request by using Rspec.

Testcase for API also return in Rspec.

Note: Skipped testcase for view/helper/decorators

## Deployed in heroku

Heroku link `https://soho-admin.herokuapp.com/` you can use same credentails as in seed to check this app in production env.

:-)
