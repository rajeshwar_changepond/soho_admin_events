require "rails_helper"

RSpec.describe CustomerEventsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/customer_events").to route_to("customer_events#index")
    end

    it "routes to #new" do
      expect(get: "/customer_events/new").to route_to("customer_events#new")
    end

    it "routes to #show" do
      expect(get: "/customer_events/1").to route_to("customer_events#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/customer_events/1/edit").to route_to("customer_events#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/customer_events").to route_to("customer_events#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/customer_events/1").to route_to("customer_events#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/customer_events/1").to route_to("customer_events#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/customer_events/1").to route_to("customer_events#destroy", id: "1")
    end
  end
end
