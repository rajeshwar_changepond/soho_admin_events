require 'rails_helper'

describe "SohoApi::V1::CustomerEvents" do

  describe "GET /api/v1/customer_events" do
    context "get index " do
      it "should return a status code 200" do
        get "/api/v1/customer_events"
        expect(response.status).to eq 200
      end
    end
  end

  describe "GET /api/v1/customer_events/{id}" do
    let(:customer) { Customer.create(email: "customer@gmail.com", password: "123456789") }
    let(:event_category) { EventCategory.create(name: "Entertainment") }
    let(:event) { Event.create(name: "Rajini movies", event_category_id: event_category.id, event_date: Time.now, event_time: Time.now, event_price: 10) }
    let(:customer_events) { CustomerEvent.create(event_id: event.id, customer_id: customer.id, booked_date: Time.now, no_of_ticket: 10, payment_mode: "card") }
    
    context " get customer with id" do
      it "should return a status code 200" do
        get "/api/v1/customer_events/#{customer_events.id}"
        expect(response.status).to eq 200
      end

      it "should return vaild email" do
        get "/api/v1/customer_events/#{customer_events.id}"
        expect(JSON.parse(response.body)["no_of_ticket"]).to eq(customer_events.no_of_ticket)
      end
    end
  end

end
