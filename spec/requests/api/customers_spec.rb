require 'rails_helper'

describe "SohoApi::V1::Customers" do

  describe "GET /api/v1/customers" do
    context "get index " do
      it "should return a status code 200" do
        get "/api/v1/customers"
        expect(response.status).to eq 200
      end
    end
  end

  describe "GET /api/v1/customers/{id}" do
    let(:customer) { Customer.create(email: "customer@gmail.com", password: "123456789") }

    context " get customer with id" do
      it "should return a status code 200" do
        get "/api/v1/customers/#{customer.id}"
        expect(response.status).to eq 200
      end

      it "should return vaild email" do
        get "/api/v1/customers/#{customer.id}"
        expect(JSON.parse(response.body)["email"]).to eq(customer.email)
      end
    end
  end

end
