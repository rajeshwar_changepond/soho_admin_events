require 'rails_helper'

RSpec.describe "Customers", type: :request do
  # authenticate customer
  let(:customer) { Customer.create(email: "customer@gmail.com", password: "123456789") }
  before(:each) do
    login_customer(customer)
  end
  after(:each) do
    logout_customer(customer)
  end
  describe "GET /customers" do
    it "works! (now write some real specs)" do
      get customers_path
      expect(response).to have_http_status(200)
    end
  end
end
