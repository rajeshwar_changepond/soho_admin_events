module ControllerLogin

  def login_customer customer
    sign_in(customer) # Using factory bot as an example
  end

  def logout_customer customer
    sign_out(customer) # Using factory bot as an example
  end

end
