class CreateCustomerHobbies < ActiveRecord::Migration[6.0]
  def change
    create_table :customer_hobbies do |t|
      t.belongs_to :customer, index: true
      t.belongs_to :hobby, index: true

      t.timestamps
    end
  end
end
