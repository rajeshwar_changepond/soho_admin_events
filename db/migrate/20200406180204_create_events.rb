class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.string :name
      t.belongs_to :event_category
      t.datetime :event_date
      t.time :event_time
      t.decimal :event_price

      t.timestamps
    end
  end
end
