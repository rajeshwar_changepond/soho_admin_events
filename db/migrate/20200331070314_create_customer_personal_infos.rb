class CreateCustomerPersonalInfos < ActiveRecord::Migration[6.0]
  def change
    create_table :customer_personal_infos do |t|
      t.belongs_to :customer, index: true
      t.integer :gender, index: true
      t.string :nationality
      t.string :address_1
      t.string :address_2
      t.string :city
      t.string :state
      t.integer :marital_status, index: true
      t.string :qualification

      t.timestamps
    end
  end
end
