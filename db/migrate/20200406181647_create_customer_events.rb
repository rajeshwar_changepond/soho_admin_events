class CreateCustomerEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :customer_events do |t|
      t.belongs_to :event
      t.belongs_to :customer
      t.datetime :booked_date
      t.integer :no_of_ticket
      t.integer :payment_mode

      t.timestamps
    end
  end
end
