class AddFamilyMemberToCustomer < ActiveRecord::Migration[6.0]
  def change
    add_reference :family_members, :customer, index: true
  end
end
