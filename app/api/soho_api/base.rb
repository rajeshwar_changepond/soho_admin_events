require 'grape-swagger'

module SohoApi
  class Base < Grape::API
    mount SohoApi::V1::Customers
    mount SohoApi::V1::CustomerEvents
    add_swagger_documentation
  end
end
