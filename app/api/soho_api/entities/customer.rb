module SohoApi
 module Entities
    class Customer < Grape::Entity
      expose :email
      expose :first_name
      expose :last_name
      expose :age
      expose :date_of_birth
    end
 end
end
