module SohoApi
 module Entities
    class Event < Grape::Entity
      expose :name
      expose :event_category do |event|
      	SohoApi::Entities::EventCategory.represent(event.event_category)
      end
      expose :event_date
      expose :event_time
      expose :event_price
    end
 end
end
