module SohoApi
 module Entities
    class CustomerEvent < Grape::Entity
      expose :event do |c_event|
      	SohoApi::Entities::Event.represent(c_event.event)
      end
      expose :customer do |c_event|
      	SohoApi::Entities::Customer.represent(c_event.customer)
      end
      expose :booked_date
      expose :no_of_ticket
      expose :payment_mode
    end
 end
end
