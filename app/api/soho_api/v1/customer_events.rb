module SohoApi
  module V1
    class CustomerEvents < Grape::API
      version 'v1', using: :path
      format :json
      prefix :api
      resource :customer_events do
        desc 'Return list of customer events'
        get do
          customers = CustomerEvent.all
          SohoApi::Entities::CustomerEvent.represent customers
        end
        desc 'Return a specific customer event'
        route_param :id do
          get do
            customer = CustomerEvent.find(params[:id])
            SohoApi::Entities::CustomerEvent.represent customer
          end
        end
      end
    end
  end
end
