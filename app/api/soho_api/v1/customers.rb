module SohoApi
  module V1
    class Customers < Grape::API
      version 'v1', using: :path
      format :json
      prefix :api
      resource :customers do
        desc 'Return list of customer'
        get do
          customers = Customer.all
          SohoApi::Entities::Customer.represent customers
        end
        desc 'Return a specific Customers'
        route_param :id do
          get do
            customer = Customer.find(params[:id])
            SohoApi::Entities::Customer.represent customer
          end
        end
      end
    end
  end
end
