module CustomerPersonalInfosHelper
  def get_customer_name customer_id
    Customer.exists?(id: customer_id) ? Customer.find(customer_id).present.name : "Something went wrong"
  end
end
