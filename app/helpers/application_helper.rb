module ApplicationHelper
  def get_customer_info_url
    current_customer.customer_personal_info.nil? ? new_customer_personal_info_path(current_customer) : customer_personal_info_path(current_customer.customer_personal_info.id)
  end
end
