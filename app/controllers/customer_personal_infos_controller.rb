class CustomerPersonalInfosController < WebController
  before_action :set_customer_personal_info, only: [:show, :edit, :update, :destroy]

  # GET /customer_personal_infos
  # GET /customer_personal_infos.json
  # def index
  #   @customer_personal_infos = CustomerPersonalInfo.all
  # end

  # GET /customer_personal_infos/1
  # GET /customer_personal_infos/1.json
  def show
  end

  # GET /customer_personal_infos/new
  def new
    @customer_personal_info = CustomerPersonalInfo.new(customer_id: current_customer.id)
  end

  # GET /customer_personal_infos/1/edit
  def edit
  end

  # POST /customer_personal_infos
  # POST /customer_personal_infos.json
  def create
    @customer_personal_info = CustomerPersonalInfo.new(customer_personal_info_params.merge({customer_id: current_customer.id}))
    respond_to do |format|
      if @customer_personal_info.save
        format.html { redirect_to @customer_personal_info, notice: 'Customer personal info was successfully created.' }
        format.json { render :show, status: :created, location: @customer_personal_info }
      else
        format.html { render :new }
        format.json { render json: @customer_personal_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customer_personal_infos/1
  # PATCH/PUT /customer_personal_infos/1.json
  def update
    respond_to do |format|
      if @customer_personal_info.update(customer_personal_info_params.merge({customer_id: current_customer.id}))
        format.html { redirect_to @customer_personal_info, notice: 'Customer personal info was successfully updated.' }
        format.json { render :show, status: :ok, location: @customer_personal_info }
      else
        format.html { render :edit }
        format.json { render json: @customer_personal_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_personal_infos/1
  # DELETE /customer_personal_infos/1.json
  # def destroy
  #   @customer_personal_info.destroy
  #   respond_to do |format|
  #     format.html { redirect_to customer_personal_infos_url, notice: 'Customer personal info was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer_personal_info
      @customer_personal_info = CustomerPersonalInfo.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def customer_personal_info_params
      params.require(:customer_personal_info).permit(:gender, :nationality, :address_1, :address_2, :city, :state, :marital_status, :qualification)
    end
end
