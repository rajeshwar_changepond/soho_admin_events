json.extract! customer_event, :id, :event_id, :customer_id, :booked_date, :no_of_ticket, :payment_mode, :created_at, :updated_at
json.url customer_event_url(customer_event, format: :json)
