json.extract! event, :id, :name, :event_category_id, :event_date, :event_time, :event_price, :created_at, :updated_at
json.url event_url(event, format: :json)
