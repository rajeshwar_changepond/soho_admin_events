json.extract! customer_personal_info, :id, :gender, :nationality, :address_1, :address_2, :city, :state, :marital_status, :qualification, :customer_id, :created_at, :updated_at
json.url customer_personal_info_url(customer_personal_info, format: :json)
