json.extract! customer, :id, :email, :first_name, :middle_name, :last_name, :age, :date_of_birth, :created_at, :updated_at
json.url customer_url(customer, format: :json)
