ActiveAdmin.register Event do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :name, :event_category_id, :event_date, :event_time, :event_price

  index do
    selectable_column
    id_column
    column :name
    column :event_category
    column :event_date
    column (:event_time) {|obj| obj.event_time.strftime( "%H:%M")}
    column :event_price
    actions
  end

  filter :name
  filter :event_category
  filter :event_date
  filter :event_price
  filter :event_time
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :event_category_id, :event_date, :event_time, :event_price]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  form do |f|
    f.inputs "Event Details" do
      f.input :name
      f.input :event_category
      f.input :event_date, as: :datepicker
      f.input :event_time, as: :time_picker
      f.input :event_price
    end
    f.actions
  end

end
