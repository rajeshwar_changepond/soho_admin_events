ActiveAdmin.register CustomerEvent do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :event_id, :customer_id, :booked_date, :no_of_ticket, :payment_mode

  index do
    selectable_column
    id_column
    column :event
    column :customer
    column :booked_date
    column :payment_mode
    actions
  end

  filter :customer
  filter :event
  filter :booked_date

  scope :all, default: true
  scope :card_payment
  scope ->{ Date.today.strftime '%A' }, :booked_date_today
  #
  # or
  #
  # permit_params do
  #   permitted = [:event_id, :customer_id, :booked_date, :no_of_ticket, :payment_mode]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  form do |f|
    f.inputs "Customer Event Details" do
      f.input :event
      f.input :customer
      f.input :no_of_ticket, as: :select, collection: [*1..20]
      f.input :payment_mode
      f.input :booked_date, as: :datepicker
    end
    f.actions
  end
end
