ActiveAdmin.register Customer do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  index do
    selectable_column
    id_column
    column :email
    column :age
    column :date_of_birth
    column :first_name
    actions
  end
  filter :email
  filter :age
  filter :date_of_birth
  # permit_params :email, :first_name, :middle_name, :last_name, :age, :date_of_birth, :reset_password_token, :reset_password_sent_at, :remember_created_at
  #
  # or
  #
  permit_params do
    permitted = [:email, :password, :password_confirmation, :first_name, :middle_name, :last_name, :age, :date_of_birth, 
                 customer_personal_info_attributes: [:id, :gender, :nationality, :address_1, :address_2, :city, :state, :marital_status, :qualification, :_destroy],
                 family_members_attributes: [:id, :name, :relationship, :role]]
    permitted << :other if params[:action] == 'create' && current_admin_user.present?
    permitted
  end

  form do |f|
    f.inputs "Customer Details" do
      f.input :email, readonly: true
      f.input :age
      f.input :date_of_birth, as: :datepicker
      tabs do
        tab 'Customer personal info' do
          f.inputs 'Customer personal info', for: [:customer_personal_info, f.object.customer_personal_info || CustomerPersonalInfo.new] do |t|
            t.input :gender
            t.input :nationality
            t.input :address_1
            t.input :address_2
            t.input :city
            t.input :state
            t.input :marital_status
            t.input :qualification
          end
        end
        tab 'Customer family Members' do
          f.has_many :family_members do |n_f|
            n_f.input :name
            n_f.input :role
            n_f.input :relationship
              # d.input :role
            end
          end
          # f.has_many :customer_families, allow_destroy: true do |a_app|
          #   a_app.inputs "Family Members" do
          #     if !a_app.object.nil?
          #       # show the destroy checkbox only if it is an existing appointment
          #       # else, there's already dynamic JS to add / remove new appointments
          #       a_app.input :_destroy, :as => :boolean, :label => "Destroy?"
          #     end

          #     a_app.inputs :family_members
          #   end
          # end
      end
    end
    f.actions
  end

  show :title => :email do |customer|

    attributes_table do
      rows :id, :first_name, :middle_name, :last_name, :age, :date_of_birth
    end

    panel 'CustomerPersonalInfo' do
      attributes_table_for customer.customer_personal_info do
        rows :gender, :nationality, :address_1, :address_2, :city, :state, :marital_status, :qualification
      end
    end

    panel 'Customerfamily' do
      attributes_table_for customer.family_members do
        rows :name, :relationship, :role
      end
    end

  end

end
