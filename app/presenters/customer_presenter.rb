class CustomerPresenter < SimpleDelegator
  def name
    first = __getobj__.first_name
    last  = __getobj__.last_name

    name = "#{first} #{last}".titleize
    name == " " ? __getobj__.email : name
  end
end
