class FamilyMember < ApplicationRecord

  enum relationship: [ :parents, :wife, :child ]
  enum role: [:father, :mother, :son, :daughter]

  validates :name, presence: true, uniqueness: true

  belongs_to :customer

end
