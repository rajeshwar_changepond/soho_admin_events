class CustomerEvent < ApplicationRecord
  belongs_to :event
  belongs_to :customer
  validates_presence_of :event, :customer, :booked_date, :payment_mode

  enum payment_mode: [:card, :cash, :upi, :wallet]
  # enum no_of_ticket: [*1..20]
  scope :card_payment,   ->    { where(payment_mode: 0) }
  scope :booked_date_today, -> { where(booked_date: Time.now) }
  # scope card_payment: { where(payment_mode: :card)}
end
