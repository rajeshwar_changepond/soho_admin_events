class CustomerPersonalInfo < ApplicationRecord
  belongs_to :customer

  enum gender: [ :male, :female, :not_sure, :prefer_not_to_disclose ]
  enum marital_status: [:single, :married, :divorced, :widowed]

end
