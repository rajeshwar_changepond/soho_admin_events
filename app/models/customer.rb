class Customer < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_one :customer_personal_info, dependent: :destroy

  has_many :customer_hobbies, dependent: :destroy
  has_many :hobbies, through: :customer_hobbies

  has_many :family_members, dependent: :destroy

  has_many :customer_events

  accepts_nested_attributes_for :family_members, :allow_destroy => true

  def present
    CustomerPresenter.new self
  end

  def display_name
    email
  end

end
