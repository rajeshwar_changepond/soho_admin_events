class Event < ApplicationRecord
  belongs_to :event_category
  has_many :customer_events
  validates_presence_of :name, :event_category, :event_date, :event_price
end
